package com.jyd.cicddemo.business.calculator;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    public Result add(int a, int b) {
        int sum = a + b;
        return new Result(sum);
    }
}
