package com.jyd.cicddemo.controllers;

import com.jyd.cicddemo.business.calculator.CalculatorService;
import com.jyd.cicddemo.business.calculator.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("add")
public class AddController {

    @Autowired
    private CalculatorService addService;

    @GetMapping
    public Result add(@RequestParam int a, @RequestParam int b) {
        return addService.add(a, b);
    }
}

